import React, { useState } from "react";
import { useQuery } from "react-query";
import { currTime } from "../../tools/axios";
import "./style/style.css";

const Register = () => {
  const [pseudo, setPseudo] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const AddUser = (e) => {
    e.preventDefault();
    console.log({ password, pseudo, email });  ///Axios add User API
  };
  const User = useQuery("User", () => currTime(), {
    refetchOnMount: "always",
    refetchOnReconnect: "always",
    refetchOnWindowFocus: "always",
  });
  
  return (
    <div className="container">
      <form className="FormSignin" onSubmit={AddUser}>
        <h1 className="TitleRegister">Enregistrer</h1>
        <label htmlFor="inputEmail" className="sr-only">
          Email address
        </label>
        <input
          className="form-control"
          placeholder="Email"
          required
          onChange={(e) => setEmail(e.target.value)}
        />
        <label htmlFor="inputPassword" className="sr-only">
          Password
        </label>
        <input
          className="form-control"
          placeholder="Password"
          required
          autoComplete="on"
          onChange={(e) => setPassword(e.target.value)}
          type="password"
        />
        <label htmlFor="inputPseudo" className="sr-only">
          Pseudo
        </label>
        <input
          className="form-control"
          placeholder="Pseudo"
          required
          onChange={(e) => setPseudo(e.target.value)}
        />
        <button className="w-100 btn btn-lg btn-primary" type="submit">
          Envoyer !
        </button>
        <div className="TimeRegister">
          Date : {new Date(User.data * 1000).toString().substring(11, 25)}
        </div>
      </form>
    </div>
  );
};

export default Register;
