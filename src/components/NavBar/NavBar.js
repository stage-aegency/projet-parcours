import React from "react"
import { Link } from "react-router-dom"
import "./style/style.css"

const NavBar = () => {
  return (
    <div >
      <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div className="container-fluid">
          <Link to="/"> Home </Link>
        </div>
        <div>
          <Link to="/Register"> Register </Link>
          <Link to="/Login"> Login </Link>
        </div>
      </nav>
    </div>
  );
};
export default NavBar;
