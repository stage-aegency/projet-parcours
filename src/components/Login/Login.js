import React, { useState } from "react";
import { useQuery } from "react-query";
import { currTime } from "../../tools/axios";
import "./style/style.css";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const LogUser = (e) => {
    e.preventDefault();
    ///Axios Log User API
    console.log({ password, email });
  };

  const User = useQuery("User", () => currTime(), {
    refetchOnMount: "always",
    refetchOnReconnect: "always",
    refetchOnWindowFocus: "always",
  });

  return (
    <div className="container">
      <form className="FormRegister" onSubmit={LogUser}>
        <h1 className="TitleLogin">Se connecter </h1>
        <label htmlFor="inputEmail" className="sr-only">
          Email address
        </label>
        <input
          className="form-control"
          placeholder="Email"
          required
          onChange={(e) => setEmail(e.target.value)}
        />
        <label htmlFor="inputPassword" className="sr-only">
          Password
        </label>
        <input
          className="form-control"
          placeholder="Password"
          required
          autoComplete="on"
          onChange={(e) => setPassword(e.target.value)}
          type="password"
        />
        <button className="w-100 btn btn-lg btn-primary" type="submit">
          Envoyer !
        </button>
        <div className="TimeLogin">
          Date : {new Date(User.data * 1000).toString().substring(11, 25)}
        </div>
      </form>
    </div>
  );
};

export default Login;
