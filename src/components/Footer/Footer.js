import React from 'react'
import "./style/style.css";

const Footer = () => {
    return (
      <footer>
        <div className="FooterContainer">
          <span className="text-footer text-muted">Footer content here.</span>
        </div>
      </footer>
    );
}

export default Footer
