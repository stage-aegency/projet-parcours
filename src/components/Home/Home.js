import React from "react";
import Loading from "../Loading/Loading"
import { useQuery } from "react-query";
import { getGeo, IPuser } from "../../tools/axios";
import "./style/style.css";

const Home = () => {
  const IP = useQuery(
    "IP",
    () =>
      IPuser(),
    {
      refetchOnMount: "always",
      refetchOnReconnect: "always",
      refetchOnWindowFocus: "always",
    }
  );

    const Geo = useQuery("Geo", () => getGeo(IP.data.ip), {
      refetchOnMount: "always",
      refetchOnReconnect: "always",
      refetchOnWindowFocus: "always",
      enabled: !!IP.data,
    });

  if (IP.isLoading || Geo.isLoading || Geo.isFetching)
    return <div className="Home"><Loading/></div>;

  return (
    <div>
      <div className="Home">Home </div>
      {IP.data ? (
        <div>
          Votre IP : {IP.data.ip}
          <br></br>
          {Geo.data ? (
            <>
              Continent : {Geo.data.data.continent_name}
              <br></br>
              Pays : {Geo.data.data.country_name}
              <br></br>
              City : {Geo.data.data.city}
              <br></br>
              <div>{console.log(Geo.data)}</div>
            </>
          ) : (
            ""
          )}
        </div>
      ) : (
        "yo"
      )}
    </div>
  );
};

export default Home;
