import "./App.css";
import { Route, Switch } from "react-router-dom";
import Home from "./components/Home/Home"
import Login from "./components/Login/Login"
import Register from "./components/Register/Register"
import NavBar from "./components/NavBar/NavBar";
import { ReactQueryDevtools } from "react-query/devtools";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div >
      <NavBar />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/Login" component={Login} />
        <Route path="/Register" component={Register} />
      </Switch>
      <Footer />
      <ReactQueryDevtools initialIsOpen />
    </div>
  );
}

export default App;
