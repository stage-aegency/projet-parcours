-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2021 at 03:23 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parcoursagent`
--
CREATE DATABASE IF NOT EXISTS `parcoursagent` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `parcoursagent`;

-- --------------------------------------------------------

--
-- Table structure for table `absences`
--

CREATE TABLE `absences` (
  `id` int(11) NOT NULL,
  `date` bigint(20) DEFAULT NULL,
  `justification` tinyint(1) DEFAULT NULL,
  `candidatID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `candidats`
--

CREATE TABLE `candidats` (
  `id` int(11) NOT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `adresse` varchar(25) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `telephone` varchar(25) DEFAULT NULL,
  `pseudoId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formationcandidat`
--

CREATE TABLE `formationcandidat` (
  `formationId` int(11) DEFAULT NULL,
  `candidatId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formations`
--

CREATE TABLE `formations` (
  `id` int(11) NOT NULL,
  `nomFormation` varchar(25) DEFAULT NULL,
  `dateDebut` bigint(20) DEFAULT NULL,
  `dateFin` bigint(20) DEFAULT NULL,
  `formationId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formationusers`
--

CREATE TABLE `formationusers` (
  `formationId` int(11) DEFAULT NULL,
  `usersId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pseudos`
--

CREATE TABLE `pseudos` (
  `id` int(11) NOT NULL,
  `pseudoNom` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `typeformations`
--

CREATE TABLE `typeformations` (
  `id` int(11) NOT NULL,
  `label` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `typeformations`
--

INSERT INTO `typeformations` (`id`, `label`) VALUES
(1, 'Integration'),
(2, 'Plateau'),
(3, 'Production');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `notif` int(1) DEFAULT NULL,
  `userstypeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `userstype`
--

CREATE TABLE `userstype` (
  `id` int(10) NOT NULL,
  `label` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userstype`
--

INSERT INTO `userstype` (`id`, `label`) VALUES
(1, 'RH'),
(2, 'Comptable'),
(3, 'Formatrice'),
(4, 'Chef plateau');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absences`
--
ALTER TABLE `absences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidatID` (`candidatID`);

--
-- Indexes for table `candidats`
--
ALTER TABLE `candidats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pseudo` (`pseudoId`);

--
-- Indexes for table `formationcandidat`
--
ALTER TABLE `formationcandidat`
  ADD KEY `formationId` (`formationId`),
  ADD KEY `candidatId` (`candidatId`);

--
-- Indexes for table `formations`
--
ALTER TABLE `formations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formationId` (`formationId`);

--
-- Indexes for table `formationusers`
--
ALTER TABLE `formationusers`
  ADD KEY `formationId` (`formationId`),
  ADD KEY `usersId` (`usersId`);

--
-- Indexes for table `pseudos`
--
ALTER TABLE `pseudos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typeformations`
--
ALTER TABLE `typeformations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userstypeID` (`userstypeID`);

--
-- Indexes for table `userstype`
--
ALTER TABLE `userstype`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absences`
--
ALTER TABLE `absences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `candidats`
--
ALTER TABLE `candidats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `formations`
--
ALTER TABLE `formations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pseudos`
--
ALTER TABLE `pseudos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `typeformations`
--
ALTER TABLE `typeformations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userstype`
--
ALTER TABLE `userstype`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `absences`
--
ALTER TABLE `absences`
  ADD CONSTRAINT `absences_ibfk_1` FOREIGN KEY (`candidatID`) REFERENCES `candidats` (`id`);

--
-- Constraints for table `candidats`
--
ALTER TABLE `candidats`
  ADD CONSTRAINT `fk_pseudo` FOREIGN KEY (`pseudoId`) REFERENCES `pseudos` (`id`);

--
-- Constraints for table `formationcandidat`
--
ALTER TABLE `formationcandidat`
  ADD CONSTRAINT `formationcandidat_ibfk_1` FOREIGN KEY (`formationId`) REFERENCES `formations` (`id`),
  ADD CONSTRAINT `formationcandidat_ibfk_2` FOREIGN KEY (`candidatId`) REFERENCES `candidats` (`id`);

--
-- Constraints for table `formations`
--
ALTER TABLE `formations`
  ADD CONSTRAINT `formations_ibfk_1` FOREIGN KEY (`formationId`) REFERENCES `typeformations` (`id`);

--
-- Constraints for table `formationusers`
--
ALTER TABLE `formationusers`
  ADD CONSTRAINT `formationusers_ibfk_1` FOREIGN KEY (`formationId`) REFERENCES `formations` (`id`),
  ADD CONSTRAINT `formationusers_ibfk_2` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userstypeID`) REFERENCES `userstype` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
